// axios({
//   url: "https://jsonplaceholder.typicode.com/users",
//   method: "post",
//   data: {
//     name: "Khoa",
//     username: "Milan",
//     email: "khoamilan123@gmail.com"
//   }
// }).then(res => console.log(res));

axios
  .delete("https://jsonplaceholder.typicode.com/users/10")
  .then(res => console.log(res))
  .catch(err => console.log(err));
